@labels = {}
File.open('input.txt', 'r') do |f|
  @line = 0
  f.each_line do |line|
    if line =~ /.*\[([A-Za-z0-9]+): ?\].*/
      @labels[/.*\[([A-Za-z0-9]+): ?\].*/.match(line)[1]] = @line
    end
    @line += 1
    @line = 0 if line.strip == "GK"
  end
end
File.open('output.txt', 'w') do |_out|
  File.open('input.txt', 'r') do |f|
    @line = 0
    @ret = 0
    f.each_line do |line|
      if line.strip =~ /T\d+K/
        @ret = line.strip.gsub!(/[^\d]+/, '').to_i
        _out << "\n" + line
      elsif line.strip == "GK"
        _out << line
        @line = 0
      else
        line = line.gsub(/S$/, 'F')
        line = line.gsub(/L$/, 'D')
        if line =~ /.*\[<([A-Za-z0-9]+)>\].*/
          label = /.*\[<([A-Za-z0-9]+)>\].*/.match(line)[1]
          if label != "self" && !@labels.include?(label)
            puts "#{@line} - \xD0\x9C\xD0\xB5\xD1\x82\xD0\xBA\xD0\xB0 " +
                     /.*\[<([A-Za-z0-9]+)>\].*/.match(line)[1] +
                     " \xD0\xBD\xD0\xB5 \xD0\xBD\xD0\xB0\xD0\xB9\xD0\xB4\xD0\xB5\xD0\xBD\xD0\xB0"
          end
          num = line.index('[<' + label)
          if label != "self"
            _out << "[#{@line}. #{@ret + @line}] #{line[0, num]}#{@labels[label]}#{line[num..line.length]}"
          else
            _out << "[#{@line}. #{@ret + @line}] #{line[0, num]}#{@line}#{line[num..line.length]}"
          end
        else
          _out << "[#{@line}. #{@ret + @line}] " + line
        end
        @line += 1
      end
    end
  end
end
