**Транслятор кваизи EDSAC в "нормальный"**

Для IO1 проверяет чтобы размер был такой как указано в начале проги (ставить обязательно!), проставляет адрес ячейки и подставляет на место метки ее адрес.
Для IO2 с относительным адресом считает еще и абсолютный. 
Квази исходник положить в input.txt и запустить нужный rb файл. Забрать результат из output.txt

---

## IO2

Автоматом исправляет окончания S->F и L->D. Служебная метка self - ставит сдрес текущей ячейки. За донат допишу чтобы self позволял арифметические операции

---

## Пример

| Вход | Выход |
|---|---|
| T64K | T64K |
| GK | GK |
| ZF | [0. 64] ZF |
| A3S | [1. 65] A3F |
| T[<exit2>]@ | [2. 66] T49[<exit2>]@ |
| A0F | [3. 67] A0F |
| T[<r>]@ | [4. 68] T50[<r>]@|
| ... | ... |
| [r:]P3D | [50. 114] [r:]P3D |

---