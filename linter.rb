@labels = {}
File.open('input.txt', 'r') do |f|
  @line = 31
  @size = 0
  f.each_line do |line|
    @size = line[1, line.length - 3].to_i if (@line == 31) && line =~ /T(\d+)S/
    if @size < @line
      puts "\xD0\xA0\xD0\xB0\xD0\xB7\xD0\xBC\xD0\xB5\xD1\x80 \xD0\xBD\xD0\xB5 "\
           "\xD1\x81\xD0\xBE\xD0\xB2\xD0\xBF\xD0\xB0\xD0\xB4\xD0\xB0\xD0\xB5" \
           "\xD1\x82"
      return
    elsif line =~ /.*\[([A-Za-z0-9]+): ?\].*/
      @labels[/.*\[([A-Za-z0-9]+): ?\].*/.match(line)[1]] = @line
    end
    @line += 1
  end
end
if @line != @size
  puts "\xD0\xA0\xD0\xB0\xD0\xB7\xD0\xBC\xD0\xB5\xD1\x80 \xD0\xBD\xD0\xB5 "\
           "\xD1\x81\xD0\xBE\xD0\xB2\xD0\xBF\xD0\xB0\xD0\xB4\xD0\xB0\xD0\xB5" \
           "\xD1\x82 " + @line.to_s
  return
end
File.open('output.txt', 'w') do |_out|
  File.open('input.txt', 'r') do |f|
    @line = 31
    f.each_line do |line|
      if (@line != 31) && line =~ /.*\[<([A-Za-z0-9]+)>\].*/
        label = /.*\[<([A-Za-z0-9]+)>\].*/.match(line)[1]
        unless @labels.include? label
          puts "\xD0\x9C\xD0\xB5\xD1\x82\xD0\xBA\xD0\xB0 " +
                   /.*\[<([A-Za-z0-9]+)>\].*/.match(line)[1] +
                   " \xD0\xBD\xD0\xB5 \xD0\xBD\xD0\xB0\xD0\xB9\xD0\xB4\xD0\xB5\xD0\xBD\xD0\xB0"
        end
        num = line.index('[<' + label)
        _out << "[#{@line}.] #{line[0, num]}#{@labels[label]}#{line[num..line.length]}"
      else
        _out << "[#{@line}.] " + line
      end
      @line += 1
    end
  end
end
